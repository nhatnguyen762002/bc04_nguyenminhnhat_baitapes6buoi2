export let renderKinh = (kinhArr) => {
  let kinhListEl = document.getElementById("vglassesList");

  kinhArr.forEach((kinh) => {
    kinhListEl.innerHTML += `<img onclick="thuKinh('${kinh.id}')" class="col-4" src="${kinh.src}" />`;
  });
};

export let showKinh = (id, kinhArr) => {
  let avatarEl = document.getElementById("avatar");

  let indexKinh = kinhArr.findIndex((kinh) => {
    return kinh.id == id;
  });

  if (indexKinh != -1) {
    avatarEl.innerHTML = `<img id="${id}" src="${kinhArr[indexKinh].virtualImg}"></img>`;
  }
};

export let showInfo = (id, kinhArr) => {
  let infoEl = document.getElementById("glassesInfo");
  infoEl.style.display = "block";
  infoEl.innerHTML = ``;

  let indexKinh = kinhArr.findIndex((kinh) => {
    return kinh.id == id;
  });

  let kinh = kinhArr[indexKinh];

  infoEl.innerHTML += `
  <h5>${kinh.name} - ${kinh.brand} (${kinh.color})</h5>
  <div class="bg-danger d-inline-flex py-1 px-2 rounded">$${kinh.price}</div>
  <span class="text-success ml-2">Stocking</span>
  <p class="mt-4">${kinh.description}</p>
  `;
};
